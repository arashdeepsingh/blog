<?php
session_start();
include_once 'app/database.php';


$db = new database();

if (isset($_SESSION['user'])) {
    $id = $_GET['pid'];
    $sql_get = "SELECT user FROM posts WHERE id = '$id'";
    $result = $db->query($sql_get);
    $row = $db->fetchArray($result);
    $postedby = $row['user'];
    if ($_SESSION['user'] != $postedby) {
        header('location:index.php');
    } else {
        $id = filter_input(INPUT_GET, 'pid');
        $sql = $db->deletePost($id);
        header('location:index.php');
    }
} else {
    header('location:login.php');
}
?>