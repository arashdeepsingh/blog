<?php
session_start();
include_once 'app/database.php';
?>
<!doctype html>
<html>
    <head>
        <title>Change Password</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" >
    </head>
    <body>
        <div align="center" class="jumbotron"><h1>Change Password</h1></div>
        <form class='form-group' action='' method='post'>
            <input placeholder='Old Password' class='form-control' type='password' name='oldpassword'>
            <input placeholder='New Password' class='form-control' type='password' name='newpassword'>
            <input class='form-control' type='submit' name='submit'>
        </form>
        <?php
        $db = new database();
        if (isset($_SESSION['user'])) {

            $uid = $_GET['uid'];
            $sessionuser = $_SESSION['user'];
            $checksql = "SELECT * FROM user WHERE username = '$sessionuser'";
            $chres = $db->query($checksql) or die('error res');
            $row = $db->fetchAssoc($chres);
            $id = $row['id'];
            if ($id != $uid) {
                header('location:index.php');
            } else {
                if (isset($_POST['submit'])) {
                    $sql = "SELECT * FROM user WHERE id = '$uid'";
                    $result = $db->query($sql);
                    $row = $db->fetchArray($result);
                    $dbpass = $row['password'];
                    $oldpass = sha1($_POST['oldpassword']);
                    $newpass = sha1($_POST['newpassword']);
                    if ($dbpass = $oldpass) {
                        $db->changePass($newpass, $id);
                        header("location:u_profile.php?uid=$uid");
                    } else {
                        echo 'Old Password Do Not Match';
                    }
                }
            }
        } else {
            header('location:index.php');
        }
        ?>
    </body>
</html>