<?php

class database {

    private $db_host;
    private $db_user;
    private $db_pass;
    private $db_name;
    private $link;

    function __construct() {
        $this->db_host = 'localhost';
        $this->db_user = 'root';
        $this->db_pass = 'root';
        $this->db_name = 'blog';
        $this->link = mysqli_connect($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        if (!$this->link) {
            echo "error in data connction";
        }
    }

    public function query($query) {
        return mysqli_query($this->link, $query);
    }

    public function fetchArray($result) {
        return mysqli_fetch_array($result);
    }

    public function fetchAssoc($result) {
        return mysqli_fetch_assoc($result);
    }

    public function numRow($result) {
        return mysqli_num_rows($result);
    }

    public function deletePost($id) {
        $query = "DELETE FROM posts WHERE id = '$id'";
        return $this->query($query);
    }

    public function deleteUser($id) {
        $query = "DELETE FROM user WHERE id = '$id'";
        return $this->query($query);
    }

    public function logInAdmin($username, $password) {
        $query = "SELECT * FROM admin WHERE username = '$username' AND password = '$password'";
        return $this->query($query);
    }

    public function logUser($username, $password) {
        $query = "SELECT * FROM user WHERE username = '$username' OR email = '$username' AND password = '$password'";
        return $this->query($query);
    }

    public function changePass($newpass, $id) {
        $query = "UPDATE user SET password = '$newpass' WHERE id = '$id'";
        $this->query($query);
    }

    public function editPost($title, $content, $id) {
        $query = "UPDATE posts SET title = '$title', content = '$content' WHERE id = '$id'";
        $this->query($query);
    }

}
