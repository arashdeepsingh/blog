<!doctype html>
<?php
session_start();
include_once '../app/database.php';
?>
<html>
    <head>
        <title>Login</title>
        <link href="../css/bootstrap.min.css" rel="stylesheet" >
    </head>
    <body>
        <form action="" method="POST" class="form-group"> 
            <input placeholder="Username" class="form-control" type="text" name="username">
            <input placeholder="Password" class="form-control" type="password" name="password">
            <input type="submit" class="form-control" name="submit">
        </form>
        <?php
        $db = new database();
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $res = $db->logInAdmin($username, $password);
            $numrow = $db->numRow($res);
            if ($numrow = 1) {
                $_SESSION['adminlogin'] = true;
                $_SESSION['adminlogin'] = $username;
                echo $_SESSION['adminlogin'];
                header('location:index.php');
            } else {
                echo "<font color='red'>Username Or Password Is Wrong</font>";
            }
        }
        ?>
    </body>
</html>
