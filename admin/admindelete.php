<?php
session_start();
include_once '../app/database.php';
include_once '../app/http.php';

$db = new database();
$http = new http();

if(isset($_SESSION['adminlogin'])){
$id = $_GET['uid'];
$deletepost = $db->deleteUser($id);
$http->location('index.php');
}else{
    $http->location('adminlog.php');
    
}