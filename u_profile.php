<?php
session_start();
include_once 'app/database.php';
?>
<!doctype html>
<html>
    <head>
        <title>Profile</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" >
        <title>Post</title>
    </head>
    <style>
        .user {
            display: inline-block;
            width: 150px;
            height: 150px;
            border-radius: 50%;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
        }

    </style>
    <body>
        <?php
        if (isset($_SESSION['user'])) {
            $db = new database();
            $uid = $_GET['uid'];
            echo "<div align='center' class='jumbotron'><h1>Your Profile</h1><a href='index.php'><button class='btn btn-primary'>Blog</button></a>&nbsp<a href='edit_profile.php?uid=$uid'><button class='btn btn-primary'>Edit profile</button><a></div>";
            $sql = "SELECT * From user WHERE id = $uid";
            $result = $db->query($sql);
            $row = $db->fetchArray($result);
            $username = $row['username'];
            $father = $row['father'];
            $mother = $row['mother'];
            $imgpath = $row['imgpath'];
            echo "<div align='center'><img class='user' src='$imgpath'><h1>Username: $username</h1><h2>Father Name: $father</h2><h2>Mother Name: $mother</h2></div>";
        } else {
            header('location:login.php');
        }
        ?>
    </body>
</html>