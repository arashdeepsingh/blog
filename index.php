<?php
session_start();
include_once 'app/database.php';
include_once 'app/http.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" >
        <title>Blog</title>
    </head>
    <body>
        <?php
        $db = new database();
        if (isset($_SESSION['user'])) {

            $puid = $_SESSION['user'];
            $idsql = "SELECT * FROM user WHERE username = '$puid'";
            $uidqueryr = $db->query($idsql);
            $uidrow = $db->fetchArray($uidqueryr);
            $uid = $uidrow['id'];
            $postsql = "SELECT * FROM posts WHERE user = '$puid'";
            $postres = $db->query($postsql);
        }
        echo '<div align="center" class="jumbotron"><h1>Developers Blog</h1>';
        if (isset($_SESSION['user'])) {
            echo "<a href='post.php'><button class='btn btn-primary'>New Post</button></a>&nbsp;";
        } else {
            echo '';
        }
        if (empty($_SESSION['user'])) {
            echo "<a href='login.php'><button class='btn btn-primary'>Login</button></a>&nbsp;
         <a href='register.php'><button class='btn btn-primary'>Register</button></a></div>&nbsp";
        } else {
            if ($db->numRow($postres) > 0) {
                echo "<a href='my_posts.php'><button class='btn btn-primary'>My Posts <span class='badge'>" . $db->numRow($postres) . "</span></button></a>&nbsp";
            }
            echo "<a href='logout.php'><button class='btn btn-primary'>Logout</button></a>&nbsp<a href='u_profile.php?uid=$uid'><button class='btn btn-primary'>Profile</button></a></div>";
        }
        $sql = "SELECT * FROM posts ORDER BY id DESC";
        $res = $db->query($sql);
        if ($db->numRow($res) > 0) {
            while ($row = $db->fetchArray($res)) {
                $id = $row['id'];
                $title = $row['title'];
                $content = $row['content'];
                $date = $row['date'];
                $postedby = $row['user'];
                $admin = "<div align='center'><a href = 'del_post.php?pid=$id'><button class='btn btn-warning'>delete</button></a>&nbsp;<a href='edit_post.php?pid=$id'><button class='btn btn-primary'>Edit</button></a></div>";

                $post = "<div align='center'><hr><h3>$id</h3><h2><a href ='view_post.php?pid=$id'>$title</a><h5>Posted By $postedby</h5></h2><h3>$date</h3><p>$content</p></div>";
                if (isset($_SESSION['user'])) {
                    if ($postedby == $_SESSION['user']) {
                        echo $post . $admin;
                    } else {
                        echo $post;
                    }
                } else {
                    echo $post;
                }
            }
        }
        ?>
    </body>
</html>
