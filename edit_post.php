<?php
session_start();
include_once 'app/database.php';
include_once 'app/http.php';


?>
<!doctype html>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" >        
        <title>Post</title>
    </head>
    <body>

        <?php
        $db = new database();
        $http = new http();
        
        if (isset($_SESSION['user'])) {
            $id = $_GET['pid'];
            $sql_get = "SELECT * FROM posts WHERE id = '$id' AND user = '" . $_SESSION["user"] . "'";
            $result = $db->query($sql_get);
            $row = $db->fetchArray($result);
            $title = $row['title'];
            $content = $row['content'];
            $postedby = $row['user'];
            if ($postedby != $_SESSION['user']) {
                $http->location('index.php');
            } else {
                if (empty($_SESSION['user'])) {
                    echo "<div align='center' class='jumbotron'><h1>You Are Not Loged In</h1><br><a href='login.php'><button class='btn btn-primary'>Login</button></a>
         <a href='register.php'><button class='btn btn-primary'>Register</button></a><div>>";
                } else {


                    echo "<div align='center' class='jumbotron'><h1>Edit Post</h1><br><a href='index.php'><button class='btn btn-primary'>Blog</button></a></div>
                    <form action='' method='post' class='form-group'>
    <input placeholder='Title' class='form-control' type='text' value ='$title' name='title'><br>
    <textarea placeholder='Content' class='form-control' name='content' rows='10' cols='25' >$content</textarea>
    <input class='form-control' type='submit' name='post'>
</form>";
                    if (isset($_POST['post'])) {
                        $title = $_POST['title'];
                        $content = $_POST['content'];
                        $db->editPost($title, $content, $id);
                        $http->location('index.php');
                    }
                }
            }
        } else {
            header('location:login.php');
        }
        ?>
        <!-- script -->
        <script src="tinymce/js/tinymce/jquery.tinymce.min.js"></script>
        <script src="tinymce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: 'textarea',
                height: 200,
                theme: 'modern',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ]
            });
        </script>
    </body>
</html>