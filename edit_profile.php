<?php
session_start();
include_once 'app/database.php';
?>
<!doctype html>
<html>
    <head>
        <title>Edit Profile</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" >        
    </head>
    <body>
        <?php
        $db = new database();
        if (isset($_SESSION['user'])) {
            $uid = $_GET['uid'];
            $sessionuser = $_SESSION['user'];
            $sql = "SELECT * FROM user WHERE username = '$sessionuser'";
            $res = $db->query($sql);
            $row = $db->fetchArray($res);
            $id = $row['id'];
            $username = $row['username'];
            if ($uid != $id) {
                header('location:index.php');
            } else {
                echo "<div align='center' class='jumbotron'><h1>Edit Account</h1></div>
<a href='change_pass.php?uid=$uid'><button class='btn btn-primary'>Change Password</button></a>";
            }
        } else {
            header('location:login.php');
        }
        ?>
    </body>
</html>

