<?php
session_start();
include_once 'app/database.php';
include_once 'app/http.php';
?>
<!doctype html>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" >
        <title>Login</title>
    </head>
    <body>
        <div align="center" class="jumbotron"><h1>Login</h1>
            <a href="index.php"><button class="btn btn-primary">Blog</button></a>
        </div>
        <form action="" method="post" class="form-group">
            <input placeholder="Username or Email" class="form-control" type="text" name="username">
            <input placeholder="Password" class="form-control" type="password" name="password">
            <input class="form-control" type="submit" name="submit">
        </form>
        <?php
        if (isset($_POST['submit'])) {
            $db = new database();
            $http = new http();
            $username = filter_input(INPUT_POST, 'username');
            $password = sha1($_POST['password']);
            $result = $db->logUser($username, $password);
            $row = $db->fetchArray($result);
            $userindb = $row['username'];
            if ($db->numRow($result) == 1) {
                $_SESSION['login'] = true;
                $_SESSION['user'] = $userindb;
                $http->location('index.php');
            } else {
                echo '<font color="red">Username Or Password Is Incorrect</font>';
            }
        }
        ?>
    </form>
</body>
</html>