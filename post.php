<?php
session_start();
include_once 'app/database.php';
?>
<!doctype html>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" >
        <title>Post</title>
    </head>
    <body>


        <?php
        $db = new database();
        if (empty($_SESSION['user'])) {
            echo '<div align="center">You are not loged in';
            echo "<a href='login.php'><button class='btn btn-primary'>Login</button></a>
         <a href='register.php'><button class='btn btn-primary'>Register</button></a></div>";
        } else {
            echo '<div align="center" class="jumbotron"><h1>Post Blog</h1><br>
                <a href="index.php"><button class="btn btn-primary">Blog</button></a></div>
                <form action="" method="post" class="forn-group">
    <input class="form-control" placeholder="Title" type="text" name="title"><br>
    <textarea class="form-control" placeholder="Content" name="content" rows="10" cols="25" ></textarea>
    <input class="form-control" placeholder="Submit" type="submit" name="post">
</form>';
            if (isset($_POST['post'])) {
                $title = $_POST['title'];
                $content = $_POST['content'];
                $postedby = $_SESSION['user'];
                $sql = "INSERT INTO posts (title, content, user)
            VALUES ('$title', '$content', '$postedby')";
                if (empty($title || $content)) {
                    echo 'Please Fill The Colums';
                } else {
                    $db->query($sql);
                    header('location: index.php');
                }
            }
        }
        ?>


        <!-- script -->
        <script src="tinymce/js/tinymce/jquery.tinymce.min.js"></script>
        <script src="tinymce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: 'textarea',
                height: 200,
                theme: 'modern',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ]
            });
        </script>

    </body>
</html>